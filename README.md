# r-place overlay

r/place overlay for coordinated groups

# Installation

Get Tampermonkey [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/) or Violentmonkey [Chrome](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en) [Firefox](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)

Click on [this link](https://gitlab.com/alifurkany/place-overlay/-/raw/main/place-overlay.user.js) and install it.

# Choosing image

In Tampermonkey/Violentmonkey, find the script and click on the "Edit" button. Then, change the `img` variable to a direct link to the image you want to use.

Example:

```javascript
const img =
	"https://gitlab.com/alifurkany/place-overlay/-/raw/main/overlay.png";
```
